//-------------------- Tipos de dados --------------------  
#define __IO volatile  // abilita RW
#define uint32_t unsigned int
#define u32 unsigned int
#define int32_t int

#define uint16_t unsigned short
#define u16 unsigned short

//--------------------  GPIOx Def --------------------
#define GPIOA ((GPIO_TypeDef * ) 0x40010800) /* GPIOx Addr Base */
#define GPIOB ((GPIO_TypeDef * ) 0x40010c00) 
#define GPIOC ((GPIO_TypeDef * ) 0x40011000) 
#define GPIOD ((GPIO_TypeDef * ) 0x40011400) 
#define GPIOE ((GPIO_TypeDef * ) 0x40011800) 
#define GPIOF ((GPIO_TypeDef * ) 0x40011c00) 
#define GPIOG ((GPIO_TypeDef * ) 0x40012000) 
/* Endereço localizado no arq sfr_100xx.asm linha 646
 *
 * para ativar um pino tem que:
 * GPIOB->ODR |= 1UL<<6 // seta "1" o bit 6 que é de 0 à 15
 */

//-------------------- TIMx Def -------------------- 
#define TIM1 ((TIM_TypeDef * ) 0x40012c00) /* TIMx Addr Base */
#define TIM2 ((TIM_TypeDef * ) 0x40000000) 
#define TIM3 ((TIM_TypeDef * ) 0x40000400) 
#define TIM4 ((TIM_TypeDef * ) 0x40000800) 
#define TIM5 ((TIM_TypeDef * ) 0x40000c00) 
#define TIM6 ((TIM_TypeDef * ) 0x40001000) 
#define TIM7 ((TIM_TypeDef * ) 0x40001400) 
#define TIM8 ((TIM_TypeDef * ) 0x40013400) 
#define TIM9 ((TIM_TypeDef * ) 0x40014C00) 
#define TIM10 ((TIM_TypeDef * ) 0x40015000) 
#define TIM11 ((TIM_TypeDef * ) 0x40015400) 
#define TIM12 ((TIM_TypeDef * ) 0x40001800) 
#define TIM13 ((TIM_TypeDef * ) 0x40001c00) 
#define TIM14 ((TIM_TypeDef * ) 0x40002000) 

//-------------------- RCC Def -------------------- 
#define RCC (( RCC_TypeDef *) 0x40021000) /* RCC Addr Base */


#define STACKINT	0x20000000
//-------------------- Area de funções ---------------  

typedef struct{
	__IO u32 CRL ; 		// 0x00 MODO de operação Port configuration register low
	__IO u32 CRH ; 		// 0x04 Port configuration register high
	__IO u32 IDR ;		// 0x08 Port input data register
	__IO u32 ODR ;		// 0x0c Port output data register
	__IO u32 BSRR ;		// 0x10 Port bit set/reset register
	__IO u32 BRR ;		// 0x14 Port bit reset register
	__IO u32 LCKR ;		// 0x18 Port configuration lock register

} GPIO_TypeDef ;

typedef struct{

	__IO u32 CR1; 		// 0x00
	__IO u32 CR2;		// 0x04
	__IO u32 SMCR;		// 0x08
	__IO u32 DIER;		// 0x0c
	__IO u32 SR; 		// 0x10
	__IO u32 EGR; 		// 0x14
	__IO u32 CCMR1;		// 0x18 ->  input/output capture mode
	__IO u32 CCMR2;		// 0x1c ->  input/output capture mode
	__IO u32 CCER; 		// 0x20 
	__IO u32 CNT; 		// 0x24
	__IO u32 PSC;		// 0x28
	__IO u32 ARR;		// 0x2c
	__IO u32 RCR;		// 0x30
	__IO u32 CCR1; 		// 0x34
	__IO u32 CCR2;		// 0x38 
	__IO u32 CCR3;		// 0x3c
	__IO u32 CCR4;		// 0x40
	__IO u32 BDTR;		// 0x44
	__IO u32 DCR;		// 0x48
	__IO u32 DMAR;		// 0x4c

} TIM_TypeDef ;

typedef struct{
//		       		  Offset
	__IO u32 CR; 		// 0x00
	__IO u32 CFGR;		// 0x04 
	__IO u32 CIR;		// 0x08
	__IO u32 APB2RSTR;	// 0x0c
	__IO u32 APB1RSTR;	// 0x10
	__IO u32 AHBENR;	// 0x14
	__IO u32 APB2ENR;	// 0x18
	__IO u32 APB1ENR;	// 0x1c
	__IO u32 BDCR;		// 0x20
	__IO u32 CSR;		// 0x24

} RCC_TypeDef;

void SystemInit (void) {

  /* Reset the RCC clock configuration to the default reset state(for debug purpose) */
  /* Set HSION bit */
  RCC->CR |= (uint32_t)0x00000001;

  /* Reset SW, HPRE, PPRE1, PPRE2, ADCPRE and MCO bits */
  RCC->CFGR &= (uint32_t)0xF8FF0000;
 
  /* Reset HSEON, CSSON and PLLON bits */
  RCC->CR &= (uint32_t)0xFEF6FFFF;

  /* Reset HSEBYP bit */
  RCC->CR &= (uint32_t)0xFFFBFFFF;

  /* Reset PLLSRC, PLLXTPRE, PLLMUL and USBPRE/OTGFSPRE bits */
  RCC->CFGR &= (uint32_t)0xFF80FFFF;

  /* Disable all interrupts and clear pending bits  */
  RCC->CIR = 0x009F0000;
 }

void Delay ( __IO u32 T ) {
	for ( T ; T > 0 ; T-- ){
		TIM3->EGR |= ( 1 << 0 );
                while ( TIM3->CNT < T ) ;
	}
}


int32_t main (void) {
	u32 Timer = 900;
// Data Of Pin [DOP_nX] -> n=numero ; X=GPIOx
//	u16 D14B, DP15B;

//
//-------------------- TIMx enable ---------------  
	// Abilita o clock RCC para TIM3
        RCC->APB1ENR |= ( 1 << 1 ); // TIM3
        TIM3->CR1 = 0x0000;

        /* fCK_PSC / (PSC[15:0] +1)) 
         * 72 Mhz / 71 + 1 = 1Mhz time clock speed 
         * logo: 25 Mhz / 24 + 1 = 1Mhz */
        TIM3->PSC = 24 ; // clock Mhz -1

        /*Definindo como valor Máx 0xffff desde que 
           verificamos manualmente
         * Se o valor atingir 1000 na função Delay */
        TIM3->ARR = 0xffff; 

        // por fim, abilitando-o
        TIM3->CR1 |= ( 1<<0 );
	


//-------------------- Sys clock 25MHz enable ---------------  
        RCC->CR |= (1 << 16); // abilita HSE
        while (!( RCC->CR & (1 << 17 ) ) ); /*Espera até a linha de 
                                              cima estar resetada 
                                              automaricamente e o 
                                              RCC->CR[17] ser setado*/
        RCC->CFGR |= (1 << 0); /* Informa a fonte de clock,
                                  no caso interno HSE */



//-------------------- RCC -> GPIOx --------------- 
	RCC->CR |= ( 1 << 16 );  /* Habilita temporariamente o HSE que 
				  se reseta sozinho */

	while (!( RCC->CR & ( 1 << 17 )) ); /* enquanto o HSE [16] for 0 e o  pino 
					  17 for 1 mantém o loop ativo (espera). */
	
	RCC->CFGR |= (1 << 0); /* seta a fonte do clock, no caso interna HSE 
				   RM0008 - P134 / p1096 */

	
//-------------------- GPIOx enable clock --------------- 
	RCC->APB2ENR |= ( 1<<2 |  1<<4 ); // GPIO A,B,C
		
   
//-------------------- Configurando I/O --------------- 
   //---------- Pino 14 -> GPIO_B ---------- |27|26|  |25|24| 14 ; 6	-> Low <= 7
//	GPIOB->CRH &= ~(0b1111 << 24);
//	GPIOB->CRH |= ( 1<<24 ); // modo pull-down | pull-up


   //---------- Pino 8 -> GPIO_A -----------    | 3| 2|  | 1| 0|  8 ; 0 > Low <= 7
	GPIOA->CRH &= ~(0b1111<<0);
	GPIOA->CRH |= ( 1<<0 ); 



   //---------- Pino 13 -> GPIO_C ---------- |23|22|-|21|20| -> High > 7
	GPIOC->CRH &= ~( 0b1111<< 20 ); 
	GPIOC->CRH |= ( 1 << 20) ; // Modo OUTPUT [1;0] 2MHz max
	GPIOC->CRH &= ~( 1 << 23 | 1 << 22) ; /* Reseta CNF [0;0] -> push-pull
      						[0;1] -> open-drain para modo != 0;0 */

   //---------- Pino 15 -> GPIO_A ---------- |31|30|-|29|28| -> Low <= 7
/*	GPIOA->CRH &= ~( 0b1111 << 28 );
   	GPIOA->CRH |= ( 1<<28 ); // modo OUTPUT 
*/
	




		
// entendendo leitura de dados - IDR
// Data Pin [DOP_nX] -> n=numero ; X=GPIOx
/*
	DP_3B |= ( GPIOB->IDR & 1<<3 );
	if ( DOP_3B == ( 1<<3 )) 
*/



  while ( 1 ) {

	Delay ( Timer );
	GPIOC->ODR ^= (1<<13);
	GPIOA->ODR ^= (1<<0);
//	GPIOB->ODR ^= ~(1<<14);
/*
	DP15B &= ~0xffffffff;
	D14B  &= ~0xffffffff;
	
	DP15B  |= ( GPIOB->IDR & 1<<31 );
	D14B   |= ( GPIOB->IDR & 1<<27 );

	if  ( D14B  == ( 1<<15 ) && Timer >= 50 )
		Timer -= 50;

	else if (DP15B == ( 1<<31 ) && Timer <= 10000)
		Timer += 50;

	else if ( Timer < 50 || Timer > 10000  ) 
			Timer = 1000;

	else 
		Timer = Timer ;


	Delay ( 500 );
	DP15B &= ~0xffffffff;
	D14B  &= ~0xffffffff;
	
	DP15B  |= ( GPIOB->IDR & 1<<31 );
	D14B   |= ( GPIOB->IDR & 1<<27 );
	
	//if ( DP15B == ( 1<<31) ) 	  
		GPIOA->ODR ^= ( 1<<28 );

	//if ( D14B == ( 1<<27))
		GPIOA->ODR ^= ( 1<<0  );
		
		
	GPIOC->ODR ^= (1<<13);
*/
  }
	


	//o // de proposito
}

/* Definições de bits "CFG" e "Mode" da configuração das GPIOs.
 *   CFG     MODE    H	 L
 * |31|30|  |29|28| 15 ; 7
 * |27|26|  |25|24| 14 ; 6	
 * |23|22|  |21|20| 13 ; 5
 * |19|18|  |17|16| 12 ; 4
 * |15|14|  |13|12| 11 ; 3
 * |11|10|  | 9| 8| 10 ; 2
 * | 7| 6|  | 5| 4|  9 ; 1
 * | 3| 2|  | 1| 0|  8 ; 0
 *
 *
 * __________________________________________
 * | 0| 0|  |01..11| <- Out Push-pull
 * | 0| 1|  |01..11| <- Out Open-drain
 * | 1| 0|  |01..11| <- Alt Out *pull
 * | 1| 0|  |01..11| <- Alt Out *drain
 * __________________________________________
 * | 0| 0|  |  00  | -< Input Analogig
 * | 0| 1|  |  00  | -< Input Floating  
 * | 1| 0|  |  00  | -< Input Pull-{down,up}
 * __________________________________________
 * */
